import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;

// Основной класс СерверМенеджер. Запускает сервер и ждет сигнала от клиентов, после чего распределяет входящие данные:
// Определяет какую команду отдал клиент: создать чат или присоединиться. Через Enum команды дает указания на создания чата или подключение к нему.

public class ServerManager {
    static Hashtable<String, Chat> ChatList = new Hashtable<>();


    public ServerManager() throws IOException {
        System.out.print("Сервер запущен\n");
        ServerSocket server = new ServerSocket(8000, 10, InetAddress.getByName("localhost"));
        int CountChat = 0;

        while (!server.isClosed()) {
            Socket NewClient = server.accept();
            CommandHandlerClass NewHandler = new CommandHandlerClass(NewClient);
            DataOutputStream out = new DataOutputStream(NewClient.getOutputStream());

            switch (NewHandler.EnumCommand) {
                case CREATE_CHAT: {
                    CountChat++;
                    Chat NewChat = new Chat(NewHandler.ChatName, CountChat);
                    if (ChatList.get(NewHandler.ChatName) == null) {
                        ChatList.put(NewHandler.ChatName, NewChat);
                        NewChat.start();
                        out.writeUTF("TRUE\n" + ChatList.get(NewHandler.ChatName).Port);
                    } else {
                        out.writeUTF(ExceptionString(EnumExceprionCode.
                                THIS_CHAT_ALREADY_CREATED));
                    }
                    break;
                }
                case CONNECT_TO_CHAT: {
                    if (ChatList.get(NewHandler.ChatName) != null) {
                        if (ChatList.get(NewHandler.ChatName).
                                ChatServer.HasClient(NewHandler.Nickname) == false) {
                            out.writeUTF("TRUE\n" + ChatList.get(NewHandler.ChatName).Port);
                            out.flush();
                        } else {
                            out.writeUTF(ExceptionString(EnumExceprionCode.NICK_IS_BUSY));
                            out.flush();
                        }
                    } else {
                        out.writeUTF("FALSE");
                        out.flush();
                    }
                    break;
                }
                default:
                    break;
            }
        }

    }

    public static String ExceptionString(EnumExceprionCode command) {
        switch (command) {
            case NICK_IS_BUSY:
                return "NICK_IS_BUSY";
            case THIS_CHAT_ALREADY_CREATED:
                return "THIS_CHAT_ALREADY_CREATED";
            default:
                return null;
        }
    }

}
