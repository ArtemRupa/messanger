import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class CommandHandlerClass {

        String Nickname;
        String ChatName;
        EnumSystemCommand EnumCommand;

        public CommandHandlerClass(Socket NewClient) throws IOException {
            DataInputStream in = new DataInputStream(NewClient.getInputStream());
            String InnerData = in.readUTF();
            Scanner scr = new Scanner(InnerData);
            String Command = scr.nextLine();
            switch(Command) {
                case "CREATE_CHAT": EnumCommand=EnumSystemCommand.CREATE_CHAT; break;
                case "CONNECT_TO_CHAT": EnumCommand=EnumSystemCommand.CONNECT_TO_CHAT; break;
                default: break;
            }
            ChatName=scr.nextLine();
            Nickname=scr.nextLine();
        }

}
