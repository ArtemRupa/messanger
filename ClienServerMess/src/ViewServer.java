import javax.swing.*;
import java.awt.*;

public class ViewServer extends JPanel{
    private Font f = new Font("Roboto", Font.BOLD, 14);
    private Font Strongf = new Font("Roboto", Font.BOLD, 25);
    private Color ColorCustomBlue = new Color(70, 130, 180);
    private Color ColorCustomWhite=new Color(255, 248, 220);
    private Color buttonTextColor = new Color(0, 51, 102);
    private Color WhiteBlueChat = new Color(204, 229, 255);

    public JButton GetButtonStop(){
        JButton ButtonStop=new JButton("PAUSE");
        ButtonStop.setBounds(100,200,100,50);
        ButtonStop.setBackground(ColorCustomBlue);
        ButtonStop.setForeground(buttonTextColor);
        ButtonStop.setFont(f);
        ButtonStop.setVisible(true);
        return ButtonStop;
    }


    public JButton GetButtonStart(){
        JButton ButtonStart=new JButton("START");
        ButtonStart.setBounds(100,100,100,50);
        ButtonStart.setBackground(ColorCustomBlue);
        ButtonStart.setForeground(buttonTextColor);
        ButtonStart.setFont(f);
        ButtonStart.setVisible(true);
        return ButtonStart;
    }


    public ViewServer(){
        JPanel serverPanel = new JPanel();
        setLayout(null);
        serverPanel.setBounds(0,0,300,300);
        serverPanel.setBackground(ColorCustomWhite);
        serverPanel.setFont(f);
        JLabel ChatGoServer = new JLabel("ChatGoServer");
        ChatGoServer.setFont(Strongf);
        ChatGoServer.setBounds(10,10,100,50);
        ChatGoServer.setForeground(buttonTextColor);
        ChatGoServer.setBackground(buttonTextColor);
        ChatGoServer.setVisible(true);
        serverPanel.add(ChatGoServer);
        serverPanel.setVisible(true);
    }
}
