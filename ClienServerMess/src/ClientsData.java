import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Scanner;

public class ClientsData {          //Общий класс для всех участников одного чата.
    //Хранит данные о чате и отвечает на запросы данных пользователей;
    //Все методы синхронизированы, так как к общему для всего чата классу обращаются множество участников

    // ДЛЯ ИСТОРИИ СООБЩЕНИЙ
    private Hashtable<Integer, String> ClientNicknameHistory = new Hashtable();     //Никнеймы тех, кто отправлял сообщения
    private Hashtable<Integer, String> ClientMessageHistory = new Hashtable();      //Сообщения от этих никнеймов

    //ДЛЯ СВЯЗИ С КЛИЕНТАМИ
    private Hashtable<String, Socket> PoolSocket = new Hashtable();                 //Пул сокетов
    private HashMap<String, Integer> NickPort = new HashMap();                      //Порты всех никнеймов
    private Hashtable<Integer, String> Nicknames = new Hashtable();                 //Все никнеймы в сессии

    //ДЛЯ ПОДСЧЕТА КОЛИЧЕСТВА КЛИЕНТОВ И СООБЩЕНИЙ
    private int ActionNum = 0;
    private int counterClient = 0;

    public synchronized void NewClient(Socket socket, String Nickname) throws IOException {

        if (PoolSocket.get(Nickname) == null) {
            NickPort.put(Nickname, socket.getPort());
            PoolSocket.put(Nickname, socket);
            Nicknames.put(counterClient, Nickname);
            counterClient++;
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            for (int i = 0; i < ClientMessageHistory.size(); i++) {                                                //Загрузка истории новому клиенту
                String NicknameSender = ClientNicknameHistory.get(i);                                          //Всего подгрузится 10 сообщений
                String MessageSender = ClientMessageHistory.get(i);
                out.writeUTF(NicknameSender + "\n" + MessageSender);
                out.flush();
                if (i == 10) {
                    break;
                }
            }
            SendMessageAllClients(Nickname, "Присоединился к беседе");
        } else {
        }
    }

    public synchronized String Messages(String NewMessage) {

        String Message = "";
        Scanner scanner = new Scanner(NewMessage);

        ClientNicknameHistory.put(ActionNum, scanner.nextLine());

        while (scanner.hasNextLine()) {
            Message = Message + scanner.nextLine();
        }

        ClientMessageHistory.put(ActionNum, Message);

        ActionNum++;


        return Message;
    }

    public synchronized String GetChatList() {
        String ChatList = "";
        for (int i = 0; i < Nicknames.size(); i++) {
            ChatList = ChatList + "\n" + Nicknames.get(i) + ": " + NickPort.get(Nicknames.get(i));
        }
        return ChatList;
    }

    public synchronized void DeleteFromChat(String Nickname) throws IOException {
        SendMessageAllClients(Nickname, Nickname + " покинул беседу.");
        PoolSocket.remove(Nickname);
        NickPort.remove(Nickname);
        for (int i = 0; i < Nicknames.size(); i++) {
            if (Nicknames.get(i).equals(Nickname) == true) {
                Nicknames.remove(i);
                System.out.println("DELETE: " + Nickname);
                break;
            }
        }
        counterClient--;

    }

    public synchronized boolean HasClient(String Nickname) {
        if (Nicknames.contains(Nickname) == false) {
            return false;
        } else {
            return true;
        }
    }

    public synchronized void SendMessageAllClients(String nickSender, String message) throws IOException {
        if (Nicknames.size() > 1) {
            for (int i = 0; i < Nicknames.size(); i++) {
                if ((!Nicknames.get(i).equals(nickSender)) && (Nicknames.get(i) != null)) {
                    String str = Nicknames.get(i);
                    Socket sock = PoolSocket.get(str);
                    if (sock != null) {
                        DataOutputStream out = new DataOutputStream(sock.getOutputStream());
                        out.writeUTF(nickSender + "\n" + message);
                        System.out.println(message);
                    }
                }
            }
        }
    }


}
