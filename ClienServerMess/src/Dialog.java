
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

//Класс обеспечивающий диалог сервера с пользователем.

public class Dialog extends Thread {

    boolean isConnect = false;
    private Socket client;
    private String Nickname;
    private DataOutputStream out;
    private DataInputStream in;
    private ClientsData ClientsData;

    private void Exit() throws IOException {
        ClientsData.DeleteFromChat(Nickname);
        client.close();
    }

    private boolean SystemCommandHandler(String message) throws IOException {
        Scanner scr = new Scanner(message);
        scr.nextLine();
        String CommandCheck = scr.nextLine();

        if (CommandCheck.equals("SYSTEM_COMMAND:")) {
            String Command = scr.nextLine();

            switch (Command) {
                case "EXIT_FROM_CHAT": {
                    Exit();
                    break;
                }
                case "GET_CHAT_LIST": {
                    String ChatList = ClientsData.GetChatList();
                    out.writeUTF("SYSTEM_COMMAND:\nGET_CHAT_LIST" + "\n" + ChatList);
                    System.out.print("\nСистемное сообщение отправлено");
                    break;
                }


            }

            return true;
        } else {
            return false;
        }
    }


    @Override
    public void run() {

        while (!client.isClosed()) {
            String message;
            try {
                message = in.readUTF();
                if (message != null && SystemCommandHandler(message) == false) {
                    message = ClientsData.Messages(message);
                    out.writeUTF(Nickname + "\n" + message);
                    System.out.println(client.getPort());
                    out.flush();
                    ClientsData.SendMessageAllClients(Nickname, message);
                    Thread.yield();
                } else {
                    Thread.yield();
                }
            } catch (Exception e) {
                try {
                    Exit();
                    break;
                } catch (IOException end) {
                    end.printStackTrace();
                }
            }
            Thread.yield();
        }

        this.interrupt();

    }


    public Dialog(Socket client, ClientsData ClientsData, String Nickname) {
        this.client = client;
        try {
            DataOutputStream out = new DataOutputStream(client.getOutputStream());
            DataInputStream in = new DataInputStream(client.getInputStream());

            ClientsData.NewClient(client, Nickname);
            System.out.println("Client " + Nickname.toUpperCase() + " connected to socket");
            this.in = in;
            this.out = out;
            this.Nickname = Nickname;
            this.ClientsData = ClientsData;
            this.isConnect = true;

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
