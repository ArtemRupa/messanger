import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Chat extends Thread {
    public String ChatName;
    public ServerSocket server;
    public String Port;
    public ClientsData ChatServer = new ClientsData();

    //Класс обеспечивает подключение к определенному чату. Родительский класс выдает чату собственный порт, на котором он ждет команд.
    //Пользователь подключается к единому хосту, а уже затем СерверМенеджер распределяет его по конкретным чатам.

    @Override
    public void run() {
        try {
            while (!server.isClosed()) {
                System.out.println("start " + ChatName);
                Socket NewClient = server.accept();
                System.out.println(NewClient.getPort());
                DataInputStream in = new DataInputStream(NewClient.getInputStream());
                DataOutputStream out = new DataOutputStream(NewClient.getOutputStream());
                String Nickname = in.readUTF();
                Dialog NewDialog = new Dialog(NewClient, ChatServer, Nickname);
                NewDialog.start();
                System.out.println("CONNECTED TO " + ChatName);

                Thread.yield();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.print("Беседа закрыта");
        }
        this.interrupt();
    }

    public Chat(String ChatName, int CountChat) throws IOException {
        this.ChatName = ChatName;
        int port = 8000 + CountChat;
        ServerSocket server = new ServerSocket(port, 10, InetAddress.getByName("localhost"));
        this.Port = Integer.toString(port);
        this.server = server;

    }
}
