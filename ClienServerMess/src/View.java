import javax.swing.*;
import java.awt.*;

public class View extends JFrame {

    Color ColorCustomBlue = new Color(70, 130, 180);
    Color buttonTextColor = new Color(0, 51, 102);


    public View() {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 300);
        setBackground(buttonTextColor);
        setResizable(false);
        setVisible(true);
        getContentPane().setBackground(ColorCustomBlue);

    }

}