import javax.swing.*;
import java.awt.event.ActionListener;

public class ModelServer {

    JButton ButtonStart;
    JButton ButtonStop;
    ThreadModelManager TMM;


    public ModelServer(){
        View view = new View();
        ViewServer panel = new ViewServer();
        JButton ButtonStart = panel.GetButtonStart();
        JButton ButtonStop = panel.GetButtonStop();
        panel.add(ButtonStart);
        panel.add(ButtonStop);
        view.add(panel);

        ButtonStart.addActionListener(new ButtonStartListener());
        ButtonStop.addActionListener(new ButtonStopListener());

        this.ButtonStart=ButtonStart;
        this.ButtonStop=ButtonStop;
    }

    // Контроллер здесь же из-за небольшого масштаба программы.

    private class ButtonStartListener implements ActionListener {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent e)  {
            try {
                ButtonStart.setEnabled(false);
                ThreadModelManager newThreadStartServer = new ThreadModelManager();
                TMM=newThreadStartServer;
                TMM.start();
            } catch (Exception end){}
        }
    }
    private class ButtonStopListener implements ActionListener{

        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
            ButtonStart.setEnabled(true);
            TMM.interrupt();
            System.out.print("Сервер остановлен\n");
        }
    }
}
