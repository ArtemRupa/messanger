import sun.util.locale.provider.AvailableLanguageTags;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

//Основной класс для работы с сервером. В конструкторе создается сокет, далее в методе CreateSocket
//осуществляется общение с сервером. В этом методе после отработки команды создается новый объект этого же класса.
//Так как важен только последний объект этого класса, его мы записываем в специальную статическую переменную.
//После чего передаем эту статическую переменную непосредственно в логику чата, где он работает с объектом, указание
//на который хранится в этой переменной.


//К объекту этого класса обращается менеджер потоков. ChatThreadManager анализирует данные на панели с помощью ModelChat
//Затем отправляет данные сюда чтоы отправить сообщения.

public class ServerConnection {

    SocketManager ActiveServer;

    private DataOutputStream out;
    private DataInputStream in;


    private void Close() throws IOException{
        in.close();
        ActiveServer.CloseSocket();
    }

    public void SendNewSystemMessage(String SYSTEM_COMMAND) throws IOException{
        if(SYSTEM_COMMAND.equals(EnumSystemMessage.EXIT_FROM_CHAT.toString())){
            Close();
            ChatThreadManager.closeThread();
        }
        out.writeUTF(ActiveServer.Nickname+"\n"+"SYSTEM_COMMAND:\n" +SYSTEM_COMMAND);
        out.flush();

    }

    public void SendNewMessage(String Message) throws IOException {
            out.writeUTF(ActiveServer.Nickname+"\n"+Message);
            out.flush();
    }

    public String InputNewMessage() throws IOException {
        String Message;
        Message = in.readUTF();
        return Message;
    }

    public ServerConnection(SocketManager ActiveServer) {
        this.ActiveServer=ActiveServer;
        try {
            this.out = new DataOutputStream(ActiveServer.GetActiveSocket().getOutputStream());
            this.in = new DataInputStream(ActiveServer.GetActiveSocket().getInputStream());
        } catch (IOException e){}
    }

}

