import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class SocketManager {


    final String ChatName;
    final String ServerName;
    final String Nickname;
    private Socket socket;
    private boolean isConnect=false;

    public Socket GetActiveSocket(){
        return socket;
    }

    public boolean isConnect(){
        return isConnect;
    }

    public void CloseSocket() throws IOException{
        socket.close();
    }

    private void CreateSocket(EnumConnectionCommand Command, int ActivePort) throws IOException {
        Socket socket = new Socket();
        socket.connect(new InetSocketAddress(ServerName, ActivePort), 200);
        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
        DataInputStream in = new DataInputStream(socket.getInputStream());

        switch (Command) {
            case CONNECT_TO_CHAT: {
                out.writeUTF("CONNECT_TO_CHAT\n"+ChatName+"\n"+Nickname);
                out.flush();
                String answer = in.readUTF();
                int Port = GetNewPortOutAnswer(answer);
                if(Port!=0) {
                    CreateSocket(EnumConnectionCommand.CALL_CHAT, Port);
                }
                break;
            }
            case CREATE_CHAT:{
                out.writeUTF("CREATE_CHAT\n"+ChatName+"\n"+Nickname);
                out.flush();
                String answer = in.readUTF();
                int Port = GetNewPortOutAnswer(answer);
                if(Port!=0) {
                    CreateSocket(EnumConnectionCommand.CALL_CHAT, Port);
                }
                break;
            }

            case CALL_CHAT: {
                out.writeUTF(Nickname);
                out.flush();
                this.socket = socket;
                isConnect=true;
                break;
            }
        }
    }

    private int GetNewPortOutAnswer(String answer){
        int Port=0;

        Scanner scr = new Scanner(answer);
        String HasChat = scr.nextLine();
        if(HasChat.equals("TRUE")){
            String getPort = scr.nextLine();
            Port = Integer.parseInt(getPort);
        } else {
            ExceptionHandler.SetException(HasChat);
            System.out.print("Подключение невозможно");
            isConnect=false;
        }

        return Port;
    }

    public SocketManager(String ServerName, int Port, String Nickname, String ChatName, EnumConnectionCommand Command) {
        this.ChatName=ChatName;
        this.Nickname=Nickname;
        this.ServerName=ServerName;
        try{ CreateSocket(Command, Port); } catch (IOException e){}

    }

}
