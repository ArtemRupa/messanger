

public class ModelLogic {

    private static final ModelLogic ModelLogicSingletonObject = new ModelLogic();
    private ModelLogic(){
        View window = new View();
        ViewPanel MenuPanel = new ViewPanel();
        ModelMenu Menu = new ModelMenu(MenuPanel);
        new ControllerMenu(Menu);

        window.add(MenuPanel);


        this.window = window;
        this.MenuPanel = MenuPanel;
    }   // private конструктор. Здесь осущесвтялется инициализация окна и главного меню;

    private View window;
    private ViewPanel MenuPanel;
    private ViewPanel AuthorizationPanel;
    private ViewPanel CreateChatMenu;
    private ViewChatPanel ChatPanel;
    private ViewSettings SettingsPanel;
    private ModelChat modelChat;

    //Логика программы. Update 18.08: реализован singletone;
    //Объкт этого класса создается единожды при открытии программы
    //Основная задача - обеспечить смену элементов JPanel

    public static ModelLogic GetInstanceML(){           //Возвращает объект класса ModelLogic
        return ModelLogicSingletonObject;
    }



    public void OpenAuthorizationMenu() {
        AuthorizationPanel = new ViewPanel();
        ModelAuthorization ModelAuthorization = new ModelAuthorization(AuthorizationPanel);
        new ControllerAuthorization(ModelAuthorization);
        MenuPanel.setVisible(false);
        window.add(AuthorizationPanel);
        AuthorizationPanel.setVisible(true);
    }

    public void OpenCreateChatMenu() {
        CreateChatMenu = new ViewPanel();
        ModelCreateChat ModelCreateChat = new ModelCreateChat(CreateChatMenu);
        new ControllerCreateChat(ModelCreateChat);
        MenuPanel.setVisible(false);
        window.add(CreateChatMenu);
        CreateChatMenu.setVisible(true);
    }

    public void OpenMenu(EnumViewLogicCommand command) {
        ModelMenu Menu = new ModelMenu(MenuPanel);
        new ControllerMenu(Menu);
        switch (command) {
            case VisibleAuthrizPanelFalse:
                AuthorizationPanel.setVisible(false);
                break;
            case VisibleCreateNewChatPanelFalse:
                CreateChatMenu.setVisible(false);
                break;
        }
        window.add(MenuPanel);
        MenuPanel.setVisible(true);
    }

    public void OpenChat(ServerConnection server, EnumViewLogicCommand command) {
        ChatPanel = new ViewChatPanel();
        modelChat = new ModelChat(ChatPanel, server);
        new ChatThreadManager(server, modelChat);
        new ControllerChat(modelChat);
        switch (command) {
            case VisibleAuthrizPanelFalse:
                AuthorizationPanel.setVisible(false);
                break;
            case VisibleCreateNewChatPanelFalse:
                CreateChatMenu.setVisible(false);
                break;
        }
        window.add(ChatPanel);
        ChatPanel.setVisible(true);
    }

    public void OpenSettingsChat() {
        SettingsPanel = new ViewSettings();
        ModelSettings modelSettings = new ModelSettings(SettingsPanel, modelChat);
        new ControllerSettings(modelSettings);
        ChatPanel.setVisible(false);
        window.add(SettingsPanel);
        SettingsPanel.setVisible(true);
    }

    public void BackToChatFromSettings() {
        SettingsPanel.setVisible(false);
        ChatPanel.setVisible(true);

    }

    public void ExitFromChat() {
        modelChat.SET_NEW_SYSTEM_MESSAGE(EnumSystemMessage.EXIT_FROM_CHAT);
        ChatPanel.setVisible(false);
        SettingsPanel.setVisible(false);
        MenuPanel.setVisible(true);
    }



}
